﻿using System;

namespace HW7T4
{
    class Program
    {
        static string OpredelenieZnaka(double a)
        {                                  
            string  b = "Число положительное";
            
            if (a < 0)
                b = "Число отрицательное";
            if (a == 0)
                b = "Число равно 0";
            return b;
        }
        static string OpredelenieProstoti(double a)
        {
            string otvet = "";
            double e = a;
            int f = Convert.ToInt32(a);
            int m = Convert.ToInt32(a);
            int Schetchik = 0;
            double b = 0;
            int c = 0;
            while (e > 0)
            {
                b = a / e;
                c = f / m;
                if (b == c)
                    {
                    Schetchik = Schetchik + 1;
                }
                e = e - 1;
                m = m - 1;
            }
            while (e < 0)
            {
                b = a / e;
                c = f / m;
                if (b == c)
                {
                    Schetchik = Schetchik + 1;
                }
                e = e + 1;
                m = m + 1;
            }
            if (Schetchik == 2)
            {
                otvet = "Число простое"; 
                    }
            else 
            {
                otvet = "Число не простое";
                    }

            return otvet;


        }
        static string OpredelenieDeleniaNacelo(double a)
        {
            string b = "";
            string c = "";
            string d = "";
            string e = "";
            string f = "";
            string otvet = "";
            if (a % 2 == 0 )
            {
                b = "Число делится на 2 нацело";
            }
            else
            {
                b = "Число делится на 2 с остатком";
            }
            if (a % 5 == 0)
            {
                c = "Число делится на 5 нацело";
            }
            else
            {
                c = "Число делится на 5 с остатком";
            }
            if (a % 3 == 0)
            {
                d = "Число делится на 3 нацело";
            }
            else
            {
                d = "Число делится на 3 с остатком";
            }
            if (a % 6 == 0)
            {
                e= "Число делится на 6 нацело";
            }
            else
            {
                e = "Число делится на 6 с остатком";
            }
            if (a % 9 == 0)
            {
                f = "Число делится на 9 нацело";
            }
            else
            {
                f = "Число делится на 9 с остатком";
            }
            otvet =$"{b}\n{c}\n{d}\n{f}";
            return otvet;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Введите Число");
            double a = double.Parse(Console.ReadLine());
            string b = OpredelenieZnaka(a);
            string c = OpredelenieProstoti(a);
            string d = OpredelenieDeleniaNacelo(a);
            Console.WriteLine(b);
            Console.WriteLine(c);
            Console.WriteLine(d);

            Console.ReadKey();

        }
    }
}
