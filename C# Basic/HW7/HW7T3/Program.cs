﻿using System;

namespace HW7T3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите сумму денег в вашей валюте");
            double a = double.Parse(Console.ReadLine());
            Console.WriteLine("Введите название вашей валюты");
            string d = Console.ReadLine();
            Console.WriteLine("Введите курс для конвертации в другую валюту");
            double b = double.Parse(Console.ReadLine());
            Console.WriteLine("Введите название валюты которую вы хотите получить");
            string e = Console.ReadLine();
            double c = a * b;
            Console.WriteLine($"{a}{d} = {c}{e}");
            Console.ReadKey();
        }
    }
}
