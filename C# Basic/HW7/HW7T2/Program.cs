﻿using System;

namespace HW7T2
{
    class Program
    {
        static double Add(double a, double b)
        {        
            return a + b;
        }
        static double Sub(double a, double b)
        {
            return a - b;
        }
        static double Mul(double a, double b)
        {
            return a * b;
        }
        static double Div(double a, double b)
        {
            return a / b;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Введите 2 числа");
            double a = double.Parse(Console.ReadLine());
            double b = double.Parse(Console.ReadLine());
            double c = 0;
            Console.WriteLine("Введите знак арифметической операции ");
            string znak = Console.ReadLine();
            switch (znak)
            {
                case "+":
                    c = Add(a, b);
                    break;
                case "-":
                    c = Sub(a, b);
                    break;
                case "*":
                    c = Mul(a, b);
                    break;
                case "/":
                    if (b == 0)
                        Console.WriteLine("Делить на ноль нельзя");
                    else
                    c = Div(a, b);
                    break;
                

            }
            
            Console.WriteLine(c);

            Console.ReadKey();

        }
    }
}
