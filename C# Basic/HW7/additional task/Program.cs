﻿using System;

namespace additional_task
{
    class Program
    {
        static double Calculate(int a, int b, int c)
        {
            return (a + b +c)/3;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Введите 3 числа");
            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());
            int c = int.Parse(Console.ReadLine());
            double e = Calculate(a, b, c);
            Console.WriteLine($"Среднее арифметическое значине аргументов = {e}");

            Console.ReadKey();

            //В условие нужно использовать 3 целочисленные значения. По этому я использовал для них int.
            //Хотя метод и "е" я объявил double, ответ все равно округляет до целого. Это можно исправить не меняя аргумент с int на double?

        }
    }
}
