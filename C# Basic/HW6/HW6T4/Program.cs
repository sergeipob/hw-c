﻿using System;

namespace HW6T4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите количество клиентов");
            int a = int.Parse(Console.ReadLine());

            int b = 1;                        
            do
            {
                b *= a--;                              
            }
            while (a > 0);

            Console.WriteLine($"Возможных маршрутов = {b}");
                        
            Console.ReadKey();
        }
    }
}
