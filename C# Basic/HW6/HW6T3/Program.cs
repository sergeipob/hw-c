﻿using System;

namespace HW6T3
{
    class Program
    {
        static void Main(string[] args)
        {
            // Прямоугольник
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    Console.Write("*");
                }
                Console.Write("\n");

            }

            // Прямоугольный треугольник
            for (int i = 0; i < 11; i++)
            {

                for (int j = 0; j < i; j++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }

            //Равносторонний треугольник
            for (int i = 0; i < 9; i++)
            {
                for (int k = 9; k > i; k--)
                {
                    Console.Write(" ");
                }
                for (int j = 0; j < i; j++)
                {
                    if (j == 1)
                        Console.Write("*");
                    if (j > 1)
                        Console.Write("**");
                }
                Console.WriteLine();
            }

            //Ромб

            for (int i = 0; i < 9; i++)
            {
                for (int k = 9; k > i; k--)
                {
                    Console.Write(" ");
                }
                for (int j = 0; j < i; j++)
                {
                    if (j == 1)
                        Console.Write("*");
                    if (j > 1)
                        Console.Write("**");
                }
                Console.WriteLine();
            }
            for (int i = 0; i < 9; i++)
            {
                for (int k = 0; k < i; k++)
                {
                    Console.Write(" ");
                }
                for (int j = 8; j > i; j--)
                {
                    if (j == 8)
                        Console.Write("*");
                    if (j < 8)
                        Console.Write("**");
                }
                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
