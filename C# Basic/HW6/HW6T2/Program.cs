﻿using System;

namespace HW6T2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите 2 произвольных числа. Первое число должно быть меньше второго");
            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());
            int c = 0;
            if (a < b)
            {
                while (b > (a + 1))
                {
                    b--;
                    c += b;

                }
                Console.WriteLine(c);
            }

            else
            {
                Console.WriteLine("Ваши значения не соответсуют условию");
            }

            // Начало второго условия данной задачи

            Console.WriteLine("Введите 2 произвольных числа. Первое число должно быть меньше второго");
            int d = int.Parse(Console.ReadLine());
            int e = int.Parse(Console.ReadLine());
            int f = 0;
            if (d < e)
            {
                while (e > (d + 1))
                {
                    e--;
                    f = e;
                    if (f % 2 != 0)
                    {
                        Console.Write($"{f}, ");
                    }
                }

            }

            else
            {
                Console.WriteLine("Ваши значения не соответсуют условию");
            }

            Console.ReadKey();
        }
    }
}