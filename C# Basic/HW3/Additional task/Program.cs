﻿using System;

namespace Additional_task
{
    class Program
    {
        static void Main(string[] args)
        {
            decimal a = 10;
            decimal b = 5;

            decimal sum = a + b;
            decimal raz = a - b;
            decimal umn = a * b;
            decimal del = b / a;
            decimal proc = b % a;

            Console.WriteLine(sum);
            Console.WriteLine(raz);
            Console.WriteLine(umn);
            Console.WriteLine(del);
            Console.WriteLine(proc);

            Console.ReadKey();
        }
    }
}
