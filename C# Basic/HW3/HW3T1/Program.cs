﻿using System;

namespace HW3T1
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 10;
            int y = 12;
            int z = 3;

            x += y - x++ * z;
            Console.WriteLine("x=" + x);
            z = --x - y * 5;
            Console.WriteLine("z=" + z);
            y /= x + 5 % z;                 
            Console.WriteLine("y="+y);
           
            z = x++ + (y * 5);
            x = y - x++ * z;

            Console.WriteLine("z2="+z);
            Console.WriteLine("x2="+x);
            
            

            Console.ReadKey();
        }
    }
}
