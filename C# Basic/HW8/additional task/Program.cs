﻿using System;

namespace additional_task
{
    class Program
    {
        static void Calculate(ref int a, ref int b, ref int c)
        {
            a = a / 5;
            b = b / 5;
            c = c / 5;
        }
            
        static void Main(string[] args)
        {
            Console.WriteLine("Введите первое значение");
            int a = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите второе значение");
            int b = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите третье значение");
            int c = int.Parse(Console.ReadLine());

            Calculate(ref a, ref b, ref c);
            Console.Write($"a= {a}\nb= {b}\nc= {c}");
            Console.ReadKey();
        }
    }
}
