﻿using System;

namespace HW8T3
{
    class Program
    {
        
        static int Factorial(int a)
        {
            if (a == 0)
                return 1;
            else
                return a * Factorial(a - 1);
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Введите количество клиентов");
            int a = int.Parse(Console.ReadLine());
            int b = Factorial(a);
            Console.WriteLine($"{b} возможных вариантов доставки");

            Console.ReadKey();

            //Для решения данной задачи не рекомендуется использовать рукурсии, так как при расчете факториала большого числа, может быть слишком большое количество рекурсий
            //а  для каждой рекурсии выделяется отдельная память, которой может не хватить.
        }
    }
}
