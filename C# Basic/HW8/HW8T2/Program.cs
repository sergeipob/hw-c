﻿using System;

namespace HW8T2
{
    class Program
    {
        
        static int RaschetKredita(int kreditsum, int month)
        {
            Console.WriteLine($"Всего внесено {kreditsum}");
            if (kreditsum <= (month * 100))
                {
                Console.WriteLine($"Сумма задолжености {month * 100 - kreditsum}");
            }
            if (kreditsum >= (month * 100))
            {
                Console.WriteLine($"Сумма переплаты {kreditsum - month * 100}");
            }
            if (kreditsum >= 700)
            {
                Console.WriteLine($"Поздравляем, кредит полностью погашен!!!");                
            }
            return kreditsum;
        }
        static void Main(string[] args)
        {
            int kreditsum = 0;
            int month = 1;            
            while (month <= 7)
            {
                Console.WriteLine($"Введите сумму погашения кредита за {month} месяц");
                int kredit = int.Parse(Console.ReadLine());
                kreditsum = kreditsum + kredit; 
                RaschetKredita(kreditsum, month);
                month++;
                if (kreditsum >= 700)
                {
                    goto Finish;
                }
            }
            Finish:
            Console.ReadKey();
        }
    }
}
