﻿using System;

namespace HW5T4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите число которое соответсвует вашему рабочему стажу (полных лет)");
            string visluga = Console.ReadLine();
            double a;
            a = Convert.ToInt32(visluga);

            if (a > 0 & a < 5)
                Console.WriteLine("Ваша выслуга составляет 10%");
            else if (a >= 5 & a < 10)
                Console.WriteLine("Ваша выслуга составляет 15%");
            else if (a >= 10 & a < 15)
                Console.WriteLine("Ваша выслуга составляет 25%");
            else if (a >= 15 & a < 20)
                Console.WriteLine("Ваша выслуга составляет 35%");
            else if (a >= 20 & a < 25)
                Console.WriteLine("Ваша выслуга составляет 45%");
            else if (a >= 25)
                Console.WriteLine("Ваша выслуга составляет 50%");
            else
                Console.WriteLine("Не существует отрицательного стажа работы. Попробуйте еще раз");

            Console.ReadKey();
        }
    }
}
