﻿using System;

namespace HW5T2
{
    class Program
    {
        static void Main(string[] args)
        {
            // Первый способ
            Console.WriteLine("Введите любое число");
            string c = Console.ReadLine();
            byte a;
            a = (byte)Convert.ToInt32(c);

           
            byte b = (byte)(a << 7);
            Console.WriteLine($"b= {b}");

            if (b == 128)
                    Console.WriteLine("Данное число не кратно 2");
            else
                Console.WriteLine("Данное число кратно 2");

            // Второй способ

            Console.WriteLine("Введите любое число");
            string k = Console.ReadLine();
            byte n = 1, m;
            int p;
            m = (byte)Convert.ToInt32(k);
            p = (int) m & n;
            if (p == 1)
                Console.WriteLine("Данное число не кратно 2");
            else
                Console.WriteLine("Данное число кратно 2");




            // Третий способ

            Console.WriteLine("Введите любое число");
            string d = Console.ReadLine();
            byte e;
            e = (byte)Convert.ToInt32(d);
            double e2 = e % 2;
            if (e2 == 0)
                Console.WriteLine("Данное число  кратно 2");
            else
                Console.WriteLine("Данное число не кратно 2");









            Console.ReadKey();
        }
    }
}
