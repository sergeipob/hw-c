﻿using System;

namespace HW4T3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите число от 0 до 100");
            string b = Console.ReadLine();
            Console.WriteLine(b);
            int a;
            a = Convert.ToInt32(b);
            
            if ( a >= 0 & a <= 14) 
            Console.WriteLine("Указанное число входит в промежуток от 0 до 15");
            else if (a >= 15 & a <= 35)
                Console.WriteLine("Указанное число входит в промежуток от 15 до 35");
            else if (a >= 36 & a <= 50)
                Console.WriteLine("Указанное число входит в промежуток от 36 до 50");
            else if (a >= 51 & a <= 100)
                Console.WriteLine("Указанное число входит в промежуток от 51 до 100");
            else
                Console.WriteLine("Указанное число не входит в промежуток от 0 до 100");

            Console.ReadKey();



        }
    }
}
