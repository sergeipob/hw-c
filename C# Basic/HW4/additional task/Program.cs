﻿using System;

namespace additional_task
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Напишите слово обозначающее погодное явление на русском языке");
            string a = Console.ReadLine();
            a = a.ToLower();

            switch (a)
            {
                case "ясно":
                    Console.WriteLine("clear");
                    break;
                case "пасмурно":
                    Console.WriteLine("Mainly cloudy");
                    break;
                case "облачно":
                    Console.WriteLine("cloudy");
                    break;
                case "дождь":
                    Console.WriteLine("rain");
                    break;
                case "гроза":
                    Console.WriteLine("thunderstorm");
                    break;
                case "метель":
                    Console.WriteLine("thunderstorm");
                    break;
                case "ураган":
                    Console.WriteLine("thunderstorm");
                    break;
                case "туман":
                    Console.WriteLine("fog");
                    break;
                case "ливень":
                    Console.WriteLine("shower");
                    break;
                case "ветренно":
                    Console.WriteLine("windy");
                    break;

                default:
                    Console.WriteLine("Просим прощения, такого слова нет в словаре");
                    break;



            }
            

            Console.ReadKey();
        }
    }
}
