﻿using System;

namespace HW9T2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите количество элементов массива");
            int a = int.Parse(Console.ReadLine());
            int[] masiv = new int[a];
            Console.WriteLine($"Ведите {a} произольных значений");
            for (int i = 0; i < a; i++)
            {
                masiv[i] = int.Parse(Console.ReadLine());
            }
            int Max = masiv[0];
            int Min = masiv[0];
            int Sum = 0;
            for (int i = 0; i < masiv.Length; i++ )
            {
                if (Max < masiv[i])
                {
                    Max = masiv[i];
                }
                if (Min > masiv[i])
                {
                    Min = masiv[i];
                }
                Sum += masiv[i]; 
            }
            double Sredn = Convert.ToDouble(Sum) / a;


            Console.WriteLine($"Максимальное значение {Max}");
            Console.WriteLine($"Минимальное значение {Min}");
            Console.WriteLine($"Сумма всех элементов массива {Sum}");
            Console.WriteLine($"Среднее значение {Sredn}");
            Console.Write("Нечетные значения:");
            for (int i=0; i < masiv.Length; i++)
            {
                if (masiv[i] % 2 != 0)
                {
                    Console.Write($" {masiv[i]}");
                }
            }
            
            Console.ReadKey();
        }
    }
}
