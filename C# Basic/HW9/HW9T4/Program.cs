﻿using System;

namespace HW9T4
{
    class Program
    {
        static int[] PlusOne(int[] arrey)
        {
            int[] newarray = new int[(arrey.Length+1)];
            for (int i = 0; i < arrey.Length; i++)
            {
                newarray[i] = arrey[i];
            }
            return newarray;
        }
        static int[] AddOne(int[] array, int value)
        {
            int[] newArray = new int[array.Length + 1];
            newArray[0] = value;
            int j = 1;
            for (int i = 0; i < array.Length; i++)
            {
                newArray[j] = array[i];
                j++;
            }

            return newArray;
        }
        static void Main(string[] args)
        {
            int[] arrey = { 3, 7, 10, 13 };
            arrey = PlusOne(arrey);
            for (int i = 0; i < arrey.Length; i++)
            {
                Console.Write($"{arrey[i]} ");
                                
            }
            Console.WriteLine();
            arrey = AddOne(arrey, 55);
            for (int i = 0; i < arrey.Length; i++)
            {
                Console.Write($"{arrey[i]} ");

            }
            
            Console.ReadKey();
        }
    }
}
