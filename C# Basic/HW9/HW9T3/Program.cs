﻿using System;

namespace HW9T3
{
    class Program
    {
        static void MyReverse(ref int[] array)
        {
            int a = array.Length;
            int[] NewArray = new int [a];
            for (int i = 0; i < array.Length; i++)
            {
                NewArray[a-1] = array[i];
                a--;
            }
            array = NewArray;
                       
        }
        static int[] SubArray(int[] array, int index, int count)
        {
            int[] NewArrey =new int[count];
            int j = 0;
            for (int i = index; i < array.Length; i++)
            {                
                NewArrey[j] = array[i];
                j++;
            }
            for (int i = j; i < count; i++)
            {
                NewArrey[i] = 1;
            }

            return NewArrey;

        }
        static void Main(string[] args)
        {
            int[] array = { 3, 5, 7, 9, 10 };
            MyReverse(ref array);
            for (int i=0; i < 5; i++)
            {
                Console.Write($"{array[i]} ");
            }
            Console.WriteLine("\n");
            int[] newArray = SubArray(array, 2, 7);
            for (int i = 0; i < newArray.Length; i++)
            {
                Console.Write($"{newArray[i]} ");
            }

            Console.ReadKey();
        }
    }
}
