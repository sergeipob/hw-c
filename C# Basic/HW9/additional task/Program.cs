﻿using System;

namespace additional_task
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = new int[10];
            for (int i=0; i < a.Length; i++)
            {
                a[i] = i;
            }
            Console.Write("Значения массива в обратном порядке:");
            for (int i=(a.Length - 1); i >= 0; i--)
            {
                Console.Write($" {a[i]}");
            }
            Console.ReadKey();
        }
    }
}
