﻿using System;

namespace HW2T2
{
    class Program
    {
        static void Main(string[] args)
        {
            double pi = 3.141592653d;
            decimal e = 2.7182818284590452m;

            Console.WriteLine(pi);
            Console.WriteLine(e);

            Console.ReadKey();
        }
    }
}
