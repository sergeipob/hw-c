﻿using System;

namespace HW2T3
{
    class Program
    {
        static void Main(string[] args)
        {
            string a = "\nмоя строка 1";
            string b = "\tмоя строка 2";
            string c = "\aмоя строка 3";

            Console.WriteLine(a);
            Console.WriteLine(b);
            Console.WriteLine(c);

            Console.ReadKey();

            // \t даже находясь внутри ковычек не отображается на экарне, а является командой к табуляции
        }
    }
}
