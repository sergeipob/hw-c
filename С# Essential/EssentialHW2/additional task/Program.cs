﻿using System;

namespace additional_task
{
    class Program
    {
        static void Main(string[] args)
        {
            User user = new User("User1", "Semen", "Semenov", 18, "13 may 2021");
            Console.Write($"Логин - {user.Login}\nИмя - {user.FirstName}\nФамилия - {user.LastName}\nВозраст - {user.Age}\nДата заполнения - {user.Date}\n");

            Console.ReadKey();
        }
    }
}
