﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace additional_task
{
    class User
    {
        public string Login { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public readonly string Date;
        public User(string Login, string FirstName, string LastName, int Age, string Date)
        {
            this.Login = Login;
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.Date = Date;
            this.Age = Age;
        }

    }
}
