﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T3
{
    class Employee
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }

        public Employee(string FirstName, string SecondName)
        {
            this.FirstName = FirstName;
            this.SecondName = SecondName;
        }
        public void Raschet(double Salary, int Experience)
        {
            double zp = 0;
            double NalSbor = 0.195;
            if (Experience > 0 & Experience < 5)
                zp = Salary*1.1;
            else if (Experience >= 5 & Experience < 10)
                zp = Salary * 1.15;
            else if (Experience >= 10 & Experience < 15)
                zp = Salary * 1.25;
            else if (Experience >= 15 & Experience < 20)
                zp = Salary * 1.35;
            else if (Experience >= 20 & Experience < 25)
                zp = Salary * 1.45;
            else if (Experience >= 25)
                zp = Salary * 1.45;
            else
                Console.WriteLine("Не существует отрицательного стажа работы. Попробуйте еще раз");
            Console.WriteLine($"Оклад с учетом стажа - {zp}");
            Console.WriteLine($"Налоговый сбор с оклада с учетом стажа  - {zp*NalSbor}");
        }
    }
    class Position
    {
        public string PositionName { get; set; }
        public double Salary { get; set; }
        public Position(string PositionName, double Salary)
        {
            this.PositionName = PositionName;
            this.Salary = Salary;
        }

    }
}
