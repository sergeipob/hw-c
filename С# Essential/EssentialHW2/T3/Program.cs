﻿using System;

namespace T3
{
    
    class Program
    {
        
        static void Main(string[] args)
        {
            Employee worker = new Employee("Ivan", "Ivanov");
            Position position = new Position("Director", 10000);
            Console.Write($"Имя - {worker.FirstName} \nФамилия - {worker.SecondName} \nДолжность - {position.PositionName}\n");
            //Стаж
            int Experience = 7;
            worker.Raschet(position.Salary, Experience);

            Console.ReadKey();
        }
    }
}
