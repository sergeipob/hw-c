﻿using System;

namespace T2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите стоимость 1 USD в гривнах");
            double Usd = double.Parse(Console.ReadLine());
            Console.WriteLine("Введите стоимость 1 EUR в гривнах");
            double Eur = double.Parse(Console.ReadLine());
            Console.WriteLine("Введите стоимость 1 RUB в гривнах");
            double Rub = double.Parse(Console.ReadLine());
            Converter converter = new Converter(Usd, Eur, Rub);           
            Console.WriteLine("Введите количество валюты в Гривнах");
            double grvn = double.Parse(Console.ReadLine());
            proverka:
            Console.WriteLine("В какую валюту хотите конвертровать Гривну? (USD, EUR, RUB)");
            String valuta = Console.ReadLine();            
            double rez = 0;
            if (valuta.ToUpper() == "USD")
            {
                rez = grvn / converter.usd;

            }
            else
            {
                if (valuta.ToUpper() == "EUR")
                {
                    rez = grvn / converter.eur;

                }
                else
                {
                    if (valuta.ToUpper() == "RUB")
                    {
                        rez = grvn / converter.rub;

                    }
                    else
                    {
                        Console.WriteLine("Вы ввели некоректную валюту. Попробуйте еще раз");
                        goto proverka;
                    }
                }
            }
                      
            Console.WriteLine($"{grvn} гривен = {rez} {valuta.ToUpper()}");

            Console.WriteLine("Введите количество валюты");
            rez = double.Parse(Console.ReadLine());
            Console.WriteLine($"{rez} USD = {rez*converter.usd} грн");
            Console.WriteLine($"{rez} EUR = {rez * converter.eur} грн");
            Console.WriteLine($"{rez} RUB = {rez * converter.rub} грн");



            Console.ReadKey();


        }
    }
}
