﻿using System;

namespace T4
{
    class Program
    {
        static void Main(string[] args)
        {
            Invoice test = new Invoice(0001, "User", "TNT");
            test.Article = "belt";
            test.Price = 500;
            test.Quantity = 3;
            test.Check(test.Quantity, test.Price);

            Console.ReadKey();

        }
    }
}
