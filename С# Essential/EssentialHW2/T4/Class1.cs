﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T4
{
    class Invoice
    {
        public readonly int account;
        public readonly string customer;
        public readonly string provider;
        public Invoice(int account, string customer, string provider)
        {
            this.account = account;
            this.customer = customer;
            this.provider = provider;
        }
        private string article;
        public string Article { get; set; }
        private int quantity;
        public int Quantity { get; set; }
        private int price;
        public int Price { get; set; }
        public void Check(int Qantity, int Price)
        {
            int check = Quantity * Price;
            double checkNDS = check * 1.2;
            Console.WriteLine($"Сумма с НДС - {checkNDS}");
            Console.WriteLine($"Сумма без НДС - {check}");
        }



    }
}
