﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T4
{
    class Point
    {                
        private int x, y;
        private string name;
                
        public int X
        {
            get { return x; }
        }

        public int Y
        {
            get { return y; }
        }

        public string Name
        {
            get { return name; }
        }
        public Point(int x, int y, string name)
        {
            this.x = x;
            this.y = y;
            this.name = name;
        }
               
    }
    class Figure
    {
        private Point[] points;
        
        public double LengthSide(Point A, Point B)
        {
            return Math.Sqrt(Math.Pow(B.Y - A.Y, 2) + Math.Pow(A.X - B.X, 2));
        }
        public void PerimeterCalculater()
        {
            double perimeter = 0;
                for (int i = 0; i < points.Length - 1; i++)
            {
                perimeter += LengthSide(points[i], points[i + 1]);
            }
            perimeter += LengthSide(points[points.Length - 1], points[0]);
            Console.WriteLine(perimeter);
        }
        public Figure(Point A, Point B, Point C)
        {
            points = new[] { A, B, C };
            Console.WriteLine("Это 3 угольник");
        }
        public Figure(Point A, Point B, Point C, Point D)
        {
            points = new[] { A, B, C, D};
            Console.WriteLine("Это 4 угольник");
        }
        public Figure(Point A, Point B, Point C, Point D, Point E)
        {
            points = new[] { A, B, C, D, E };
            Console.WriteLine("Это 5 угольник");
        }
        
    }
}
        
