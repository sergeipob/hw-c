﻿using System;

namespace T4
{
    class Program
    {       
        static void Main(string[] args)
        {
            
            Point A = new Point(10, 15, "A");
            Point B = new Point(20, 20, "B");
            Point C = new Point(25, 10, "C");
            Point D = new Point(17, 14, "D");
            Point E = new Point(25, 13, "E");

            Figure obj = new Figure(A, B, C, D, E);
            obj.PerimeterCalculater();
            
            Console.ReadKey();
        }
    }
}
