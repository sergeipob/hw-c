﻿using System;

namespace additional_task
{
    class Address
    {
        private string index;

        public void SetIndex(string value) 
        {
            index = value;
        }

        public string GetIndex()           
        {
            return index;
        }
        private string country;

        public void SetCountry(string value)
        {
            country = value;
        }

        public string GetCountry()
        {
            return country;
        }
        private string city;

        public void SetCity(string value)
        {
            city = value;
        }

        public string GetCity()
        {
            return city;
        }
        private string street;

        public void SetStreet(string value)
        {
            street = value;
        }

        public string GetStreet()
        {
            return street;
        }
        private string house;

        public void SetHouse(string value)
        {
            house = value;
        }

        public string GetHouse()
        {
            return house;
        }
        private string apartment;

        public void SetApartment(string value)
        {
            apartment = value;
        }

        public string GetApartment()
        {
            return apartment;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Address Home = new Address();
            Home.SetIndex("39785");
            Console.WriteLine($"Index - {Home.GetIndex()}");
            Home.SetCountry("Ukraine");
            Console.WriteLine($"Country - {Home.GetCountry()}");
            Home.SetCity("Mariupol");
            Console.WriteLine($"City - {Home.GetCity()}");
            Home.SetStreet("Shevchenko");
            Console.WriteLine($"Street - {Home.GetStreet()}");
            Home.SetHouse("91");
            Console.WriteLine($"House - {Home.GetHouse()}");
            Home.SetApartment("81");
            Console.WriteLine($"Apartment - {Home.GetApartment()}");

            Console.ReadKey();

        }
    }
}
