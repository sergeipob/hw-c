﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T3
{
    class Title
    {
        public string content;

        public string Content
        {
            private get
            {
                if (content != null)
                    return content;
                else
                    return "Название книги отсутствует.";
            }
            set
            {
                content = value;
            }
        }

        public void Show()
        {
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine(Content);
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }
    class Author
    {
        public string content;

        public string Content
        {
            private get
            {
                if (content != null)
                    return content;
                else
                    return "Имя автора отсутствует.";
            }
            set
            {
                content = value;
            }
        }

        public void Show()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(Content);
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }
    class Content2
    {
        public string content;

        public string Content
        {
            private get
            {
                if (content != null)
                    return content;
                else
                    return "Содержание отсутствует.";
            }
            set
            {
                content = value;
            }
        }

        public void Show()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(Content);
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }
    class Book
    {
        Title title = null;
        Author author = null;
        Content2 contеnt = null;

        void InitializeBook()
        {
            this.title = new Title();
            this.author = new Author();
            this.contеnt = new Content2();
        }

        public Book()
        {
            InitializeBook();
            
        }
        public string Autor
        {
            set
            {
                this.author.Content = value;
            }
        }
        public string Title
        {
            set
            {
                this.title.Content = value;
            }
        }
        public string Content
        {
            set
            {
                this.contеnt.Content = value;
            }
        }
        public void Show()
        {
            this.title.Show();
            this.author.Show();
            this.contеnt.Show();
        }
    }
}
