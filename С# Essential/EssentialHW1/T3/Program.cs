﻿using System;

namespace T3
{


    class Program
    {
        static void Main(string[] args)
        {
            Book book = new Book();
            Console.WriteLine("Введите название книги");
            book.Title = Console.ReadLine();
            Console.WriteLine("Введите имя автора");
            book.Autor = Console.ReadLine();
            Console.WriteLine("Введите содержание книги");
            book.Content = Console.ReadLine();

            book.Show();

            Console.ReadKey();
        }
    }
}
