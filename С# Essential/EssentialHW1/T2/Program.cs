﻿using System;

namespace T2
{
    class Rectangle
    {
        private double side1, side2;
        
        public double AreaCalculator(double side1, double side2)
        {
            double S = side1 * side2;
            return S;
        }
        public double PerimeterCalculator(double side1, double side2)
        {
            double P = 2 * (side1 + side2);
            return P;
        }
        public double Area()
        {
            return AreaCalculator(side1, side2);
        }
        public double Perimeter()
        {
            return PerimeterCalculator(side1, side2);
        }
        public Rectangle(double side1, double side2)
        {
            this.side1 = side1;
            this.side2 = side2;
        }
    }
    
    class Program
    {

        static void Main(string[] args)
        {
            Console.WriteLine("Введите значение стороны прямоугольника");
            double a = double.Parse(Console.ReadLine());
            Console.WriteLine("Введите значение второй стороны прямоугольника");
            double b = double.Parse(Console.ReadLine());
            Rectangle instance = new Rectangle(a, b);
            Console.WriteLine($"Площадь - {instance.Area()}");
            Console.WriteLine($"Периметр - {instance.Perimeter()}");

            Console.ReadKey();

        }
    }
}
